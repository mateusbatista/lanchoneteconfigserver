package br.unisul.LanchoneteConfigServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanchoneteConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LanchoneteConfigServerApplication.class, args);
	}
}
